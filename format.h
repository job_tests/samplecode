#ifndef __LOGS_STRING_FORMAT_H__
#define __LOGS_STRING_FORMAT_H__
/**
 * @file format.h
 * @author Dmitriy Gertsog (dmyger@gmail.com)
 *
 * @brief C++ string format helper
 *
 * Format string use '%' as placeholder for any params; Use '%' in params to output a percent symbol.
 *     Note: Can be redifined with LOG_FORMAT_SPLITTER_CHAR
 *
 * Formatted objects should has implemented 'operator<<'.
 *   friend std::ostream &operator<<(std::ostream &output, YourClass const &obj) { ...; return output; }
 *
 * Samples of usage:
 * using SFMT = ::Log::Format;
 *
 * std::string str(SFMT("Format string: (1=%, 2=%%%%, 3=%%, 4=%) Completed", ptr, std::setw(10), std::setfill('_'), 20l, '%', std::scientific, 3.1415, "End string"));
 *     *>> Format string: (1=0x1a2b3c, 2=________20%, 3=3.141500e+00, 4=End string) Completed
 *
 * std::cout << SFMT(ptr, 2l, 3.1415f);  // Only params, no any formats
 *     *>> 0x123456; 2; 3.1415
 *
 * SFMT("With extra params more than formated: 1=%%, 2=%, ", std::boolalpha, is_v, 20l, 3.1415, "End message");
 *     *>> With extra params more than formated: 1=true, 2=20, 3.1415; End message
 *
 * SFMT("Text without any format=", 1, 2l, 3.1415f);
 *     *>> Text without any format=1; 2; 3.1415
 *
 * SFMT("Only Message");
 *     *>> Only Message
 *
 * More complex usage. Useful with exceptions:
	class SomeException : public std::runtime_error {
		public:
			SomeException(::Log::Format fmt)
				: std::runtime_error(fmt)
			{}
	};
	...
	throw SomeException({"Error (%) with file='%'", strerror(errno), filename})
 *
 * Or other methods, which accept Log::Format
 	void print(::Log::Format const &fmt) {
		std::cout << fmt;
	}
	...
	print({"At 0x% count=%; average=%", ptr, 2l, 3.1415f});
 *	    *>> At 0x123456 count=2; average=3.1415"
 *
	void doSomething(::Log::Format const &fmt) {
		std::string str(fmt);
		// do something with the `str`...
	}
	...
	doSomething({"% <=> %", val1, val2});
 */
#include <sstream>
#include <string>
#include <string.h>
#include "helpers.h"

#ifndef LOG_FORMAT_SPLITTER_CHAR
#define LOG_FORMAT_SPLITTER_CHAR '%'
#endif

namespace Log {

class Format
{
public:
	template<typename... Ts>
	Format(char const *format, Ts const &... args)
	    : sline()
	{
		fmt(format, args...);
	}

	template<typename T, typename... Ts>
	Format(T const &arg1, Ts const &... args)
	{
		sline << arg1;
		arg(args...);
	}

	operator std::string() const { return sline.str(); }

	friend std::ostream &operator<<(std::ostream &output, Format const &fmt)
	{
		output << fmt.sline.str();
		return output;
	}

private:
	template<typename T, typename... Ts>
	void fmt(char const *format, T const &arg1, Ts const &... args);

	template<typename T, typename... Ts>
	void arg(T const &arg1, Ts const &... args);

	void fmt(char const *format) { sline << format; }
	void arg() {}

	std::stringstream sline;
};

template<typename T, typename... Ts>
void Format::fmt(char const *format, T const &arg1, Ts const &... args)
{
	char const *arg_pos = *format ? strchr(format, LOG_FORMAT_SPLITTER_CHAR) : nullptr;
	if(arg_pos) {
		sline.write(format, arg_pos - format);
		sline << arg1;
		// TODO(dmyger@gmail.com): here may be code to skip printf formats
		fmt(arg_pos + 1, args...);
	} else if(*format) {
		sline << format << arg1;
		arg(args...);
	} else
		arg(arg1, args...);
}

template<typename T, typename... Ts>
void Format::arg(T const &arg1, Ts const &... args)
{
	sline << "; " << arg1;
	arg(args...);
}

}  // namespace Log

#endif  // !__LOGS_STRING_FORMAT_H__
