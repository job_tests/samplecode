#ifndef __COMMON_SETTINGS_ENTRY_H__
#define __COMMON_SETTINGS_ENTRY_H__
#include <cassert>
#include <string>
#include <time.h>
#include <type_traits>
#include <typeindex>
#include <vector>

#include "utils/is_vector.h"
#include "reader/reader.h"
#include "exceptions.h"

namespace Config {

class SettingsGroup;

template<typename T>
struct Init
{
	SettingsGroup *parent;
	char const *key;
	T def_value;
	std::type_index const type_index;
};

template<typename T>
struct InitMinMax
{
	Init<T> base;
	T min;
	T max;
};

template<typename T>
struct InitInterpolate
{
	Init<T> base;
	int valid_seconds;
};

class MinMaxRangeError : public Exception
{
public:
	MinMaxRangeError(std::string const &key)
	    : Exception("Out of Min/Max range for ")
	{
		message += key;
	}
};

class SettingsEntryBase
{
public:
	virtual ~SettingsEntryBase() {}

	bool isValidType(std::type_index const other_ti) const { return type_index == other_ti; }

	char const *getTypeName() const
	{
		return type_index.name();
	}

	virtual void update(Reader *settings_reader) = 0;
	virtual void reset() = 0;
	virtual std::string dump() const = 0;

protected:
	SettingsEntryBase(std::type_index const ti)
	    : type_index(ti)
	{}

	std::type_index const type_index;
};

template<typename T>
class SettingsEntry : public SettingsEntryBase
{
public:
	SettingsEntry(Init<T> init)
	    : SettingsEntryBase(init.type_index)
	    , parent(*init.parent)
	    , def_value(init.def_value)
	    , value()
	    , has_value()
	{
		if(init.parent)
			init.parent->add_entry(init.key, this);
	}

	virtual T const &operator()() const { return get(); }
	virtual void operator()(T new_value) { set(new_value); }

	virtual T const &get() const { return has_value ? value : def_value; }

	virtual void set(T new_value)
	{
		value = new_value;
		has_value = true;
	}
	virtual void update(Reader *settings_reader) override;
	virtual void reset() override { has_value = false; }

	virtual std::string dump() const override { return to_string(get()); }

protected:
	virtual void updateValue(std::string const &key, Reader *settings_reader);

	template<class S, typename = std::enable_if_t<std::is_base_of<std::string, S>::value>>
	std::string to_string(S const &v) const
	{
		return std::string('"' + v + '"');
	}

	template<typename S>
	typename std::enable_if_t<std::is_arithmetic<S>::value && !std::is_same<S, bool>::value, std::string> to_string(
	    S v) const
	{
		return std::to_string(v);
	}

	template<typename S>
	typename std::enable_if_t<std::is_same<S, bool>::value, std::string> to_string(S v) const
	{
		return std::string(v ? "true" : "false");
	}

	template<typename S>
	typename std::enable_if_t<std::is_enum<S>::value, std::string> to_string(S v) const
	{
		using EnumType = typename std::underlying_type<T>::type;
		return std::to_string(EnumType(v));
	}

	template<typename S>
	typename std::enable_if_t<Utils::is_vector_v<S>, std::string> to_string(S v) const
	{
		std::string result(1, '[');
		for(auto &val : v) {
			if(result.length() > 1)
				result += ", ";
			result += to_string(val);
		}
		result.append("]");
		return result;
	}

	SettingsGroup const &parent;
	T const def_value;
	T value;
	bool has_value;
};

template<typename T>
class SettingsEntryMinMax : public SettingsEntry<T>
{
public:
	SettingsEntryMinMax(InitMinMax<T> init)
	    : SettingsEntry<T>(init.base)
	    , min(init.min)
	    , max(init.max)
	{}

	virtual void set(T new_value) override
	{
		if(min < max) {
			if(new_value < min || max < new_value) {
				throw MinMaxRangeError(SettingsEntry<T>::parent.get_key(this));
			}
		}
		SettingsEntry<T>::set(new_value);
	}

protected:
	T min;
	T max;
};

template<class T, typename = std::enable_if_t<std::is_base_of<std::string, T>::value>>
class SettingsEntryInterpolate : public SettingsEntry<T>
{
public:
	SettingsEntryInterpolate(InitInterpolate<T> init)
	    : SettingsEntry<T>(init.base)
	    , valid_seconds(init.valid_seconds)
	    , last_value()
	    , last_interpolate()
	{}

	virtual T const &get() const override
	{
		if(needValueUpdate()) {
			last_value = SettingsEntry<T>::get();
			interpolateValue(&last_value);
		}
		return last_value;
	}

	virtual void set(T new_value) override
	{
		last_value.clear();
		SettingsEntry<T>::set(new_value);
	}

private:
	bool needValueUpdate() const
	{
		timespec now;
		clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
		if(last_value.empty() || (now.tv_sec - last_interpolate) > valid_seconds) {
			last_interpolate = now.tv_sec;
			return true;
		}
		return false;
	}

	int const valid_seconds;
	std::string mutable last_value;
	time_t mutable last_interpolate;
};

}  // namespace Config

#include "groups.h"

namespace Config {

template<typename T>
/*virtual*/ void SettingsEntry<T>::update(Reader *settings_reader)
{
	std::string key = parent.get_key(this);
	try {
		updateValue(key, settings_reader);
	} catch(ReaderNoValue const &e) {
		reset();
	}
}

template<typename T>
void SettingsEntry<T>::updateValue(std::string const &key, Reader *settings_reader)
{
	T new_value = T();
	*settings_reader >> Key(key) >> new_value;
	set(new_value);
}

}  // namespace Config

#endif  // !__COMMON_SETTINGS_ENTRY_H__
