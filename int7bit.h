#ifndef __UTILS_INT_7BIT_CODING_H__
#define __UTILS_INT_7BIT_CODING_H__
#include <cinttypes>
#include <type_traits>
#include <stdexcept>

namespace Utils {

class int7b
{
public:
	template<class T, typename = std::enable_if_t<std::is_integral<T>::value>>
	int7b(void *buffer, T value)
	    : buffer(static_cast<uint8_t *>(buffer))
	    , size_of()
	    , is_const()
	{
		set(value);
	}

	template<typename T>
	int7b(T *buffer)
	    : buffer(const_cast<uint8_t *>(reinterpret_cast<uint8_t const *>(buffer)))
	    , size_of()
	    , is_const(std::is_const<T>::value)
	{}

	template<class T, typename = std::enable_if_t<std::is_integral<T>::value>>
	int set(T value)
	{
		if(is_const)
			throw std::logic_error("Can't modify const object");
		auto val = prepare_value(value);
		size_of = 0;
		while(val > 0x7F) {
			buffer[size_of++] = static_cast<uint8_t>((val & 0x7F) | 0x80);
			val >>= 7;
		}
		buffer[size_of++] = static_cast<uint8_t>(val);
		return size_of;
	}

	template<class T, typename = std::enable_if_t<std::is_integral<T>::value>>
	T get(int *const value_size = nullptr) const
	{
		using uint_type = typename std::make_unsigned_t<T>;
		size_of = 0;
		size_t const max_shift = (sizeof(T) + 1) * 7;
		uint_type result = 0;
		for(size_t shift = 0; shift <= max_shift; shift += 7) {
			uint8_t const c = buffer[size_of++];
			result |= static_cast<uint_type>(c & 0x7f) << shift;
			if((c & 0x80) == 0) {
				if(value_size)
					*value_size = size_of;
				if(std::is_signed<T>())
					return static_cast<T>((result & 1) ? ~(result >> 1) : (result >> 1));
				return result;
			}
		}
		throw std::overflow_error("Value does not fit in type");
	}

	int size() const
	{
		if(size_of > 0)
			return size_of;
		size_of = 0;
		while(buffer[size_of] & 0x80)
			++size_of;
		return ++size_of;
	}

	template<class T, typename = std::enable_if_t<std::is_integral<T>::value>>
	static int calcSize(T value)
	{
		if(!value)
			return 1;
		auto val = prepare_value(value);
		int required_size = 0;
		while(val) {
			++required_size;
			val >>= 7;
		}
		return required_size;
	}

private:
	template<class T, typename = std::enable_if_t<std::is_signed<T>::value>>
	static std::make_unsigned_t<T> prepare_value(T value)
	{
		return value >= 0 ? (value << 1) : ~(value << 1);
	}

	template<class T, typename = std::enable_if_t<std::is_unsigned<T>::value>>
	static T prepare_value(T value)
	{
		return value;
	}

	uint8_t *const buffer;
	mutable int size_of;
	bool const is_const;
};

}  // namespace Utils

#endif  // !__UTILS_INT_7BIT_CODING_H__
