#ifndef __IPC_CHANNEL_ENCODING_H__
#define __IPC_CHANNEL_ENCODING_H__

#include <cassert>
#include <cstddef>
#include <memory>
#include <vector>
#include "rpc/ipc/IpcException.h"
#include "rpc/ipc/Sender.h"

namespace ipc {

enum class TransferMode
{
	NONE,
	BINARY,
	JSON
};
std::ostream &operator<<(std::ostream &output, TransferMode mode);

namespace encoding {

class Context
{
public:
	virtual ~Context() {}
	virtual void reset() = 0;
};

using ContextPtr = std::unique_ptr<Context>;

class CodingWithContext
{
public:
	CodingWithContext()
	    : context()
	{}

	bool hasContext() noexcept { return bool(context); }

	template<typename T>
	void setContext(T &&new_context) noexcept
	{
		context = std::forward<T>(new_context);
	}

	template<typename T = Context>
	T &getContext() noexcept
	{
		assert(hasContext() && "Wrong flow! Context should be initialized before this call");
		return dynamic_cast<T &>(*context);
	}

private:
	ContextPtr context;
};

template<ipc::TransferMode M>
struct As
{
	static ipc::TransferMode constexpr mode = M;
};

template<ipc::TransferMode M, class T>
struct To
{
	static ipc::TransferMode constexpr mode = M;
	using Type = T;
};

}  // namespace encoding

class ChannelEncoder : public encoding::CodingWithContext
{
public:
	ChannelEncoder(SenderPtr sender)
	    : encoding::CodingWithContext()
	    , sender(sender)
	{}

	virtual ~ChannelEncoder() {}
	virtual TransferMode mode() const noexcept = 0;
	virtual void append(void const *data, size_t len) = 0;

	template<TransferMode mode, typename T>
	ChannelEncoder &save(T &&val)
	{
		encodeFrom(*this, std::forward<T>(val), encoding::As<mode>{});
		return *this;
	}

	template<typename T>
	ChannelEncoder &operator<<(T &&val)
	{
		switch(mode()) {
			case TransferMode::BINARY: save<TransferMode::BINARY>(std::forward<T>(val)); break;
			case TransferMode::JSON: save<TransferMode::JSON>(std::forward<T>(val)); break;
			default: throw Exception("Unknown encoder transfer mode");
		}
		return *this;
	}

protected:
	SenderPtr sender;
};

class ChannelDecoder : public encoding::CodingWithContext
{
public:
	ChannelDecoder()
	    : encoding::CodingWithContext()
	    , counter()
	{}

	virtual ~ChannelDecoder() {}
	virtual TransferMode mode() const noexcept = 0;
	virtual char const *end() const noexcept = 0;
	virtual int length() const noexcept = 0;
	virtual void shift(void const *ptr) = 0;
	virtual void shift(size_t element_size) = 0;

	template<typename T, typename F = void (*)(T &&)>
	void iterate(F callback)
	{
		while(read()) {
			callback(load<T>());
			++counter;
		}
	}

	template<typename T>
	std::vector<T> getList()
	{
		std::vector<T> list;
		iterate([&list](T &&object) { list.push_back(std::move(object)); });
		return list;
	}

	template<TransferMode mode, typename T>
	T load()
	{
		return decodeAs(*this, encoding::To<mode, T>{});
	}

	template<typename T>
	T load()
	{
		switch(mode()) {
			case TransferMode::BINARY: return load<TransferMode::BINARY, T>();
			case TransferMode::JSON: return load<TransferMode::JSON, T>();
			default: break;
		}
		throw Exception("Unknown encoder transfer mode");
	}

	int count() const noexcept { return counter; }

protected:
	virtual bool read() = 0;

private:
	int counter;
};

using EncodeAsBinary = ipc::encoding::As<ipc::TransferMode::BINARY>;
template<typename T>
using DecodeBinaryTo = ipc::encoding::To<ipc::TransferMode::BINARY, T>;

using EncodeAsJson = ipc::encoding::As<ipc::TransferMode::JSON>;
template<typename T>
using DecodeJsonTo = ipc::encoding::To<ipc::TransferMode::JSON, T>;

}  // namespace ipc

using IpcEncoderPtr = std::unique_ptr<ipc::ChannelEncoder>;
using IpcDecoderPtr = std::unique_ptr<ipc::ChannelDecoder>;

#endif  // !__IPC_CHANNEL_ENCODING_H__
