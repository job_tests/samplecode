# Some examples of my code
## ChannelEncoding.h
Base class used in IPC/RPC protocols to serialize/deserialize data.
## entry.h
Class used to keep application settings entry. Support reading from file with external reader.
## format.h
Helper to produce formatted strings from C++ objects. Can be used it self, but mostly for logger subsystem.
## int7bit.h
Encoder/Decoder of integer data with variable length encoding.
